var mjAPI = require("mathjax-node-svg2png"),
    PNG = require('pngjs').PNG,
    fs = require('fs'),
    stream = require('stream');

var request = require("request")

mjAPI.config({
    MathJax: {
        SVG: {
            linebreaks: true
        }
    }
});

function generateImage(yourMath , query, cb){
    var format = query.f || "AsciiMath";

    var width = query.w || 320;

    var scale = query.s || 2;

    mjAPI.typeset({
        math: yourMath,
        format: format,
        png:true,
        scale:scale,
        width: width
    }, function (data) {

        var img = new Buffer(data.png.substr(data.png.indexOf(',')), 'base64');
        if(query.t && query.t=='true') {
            cb(img)
            return;
        }

        var png = PNG.sync.read(img);
        var options = {
            colorType: 2,
            bgColor : {
                red: 255,
                green: 255,
                blue: 255
            }
        };
        var ret = PNG.sync.write(png,options);
        cb(ret)

    });
}


exports.buildImage = function(query, cb){

    if( query.e && query.e.length != 0 ){
        console.log("e")
        return generateImage(decodeURIComponent(query.e), query, cb)
    }

    if( query.u && query.u.length != 0){
        request({
            url: query.u,
            text: true
        }, function (error, response, body) {
            console.log("u")
            if (!error && response.statusCode === 200) {
                generateImage(body, query,cb);
            }else{
                console.log(error);
            }
        })
        return;
    }
    console.log("nada")
    return generateImage('E=m*c^2', query, cb)
}