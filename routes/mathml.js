var express = require('express');
var router = express.Router();
var math2ml = require('./mathml2png')

router.get('/', function(req, res, next) {
    res.render('form', { title: 'MathML to PNG' });
});

router.get('/mathml.png', function(req, res, next) {

    math2ml.buildImage(req.query, function(img){
        res.writeHead(200, {
            'Content-Type': 'image/png',
            'Content-Length': img.length
        });
        res.end(img);
    })


});

module.exports = router;
