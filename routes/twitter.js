
var math2ml = require('./mathml2png')
var twitterAPI = require('node-twitter-api');

var express = require('express');
var router = express.Router();

var twitter = new twitterAPI({
    consumerKey: process.env.TWITTER_CONSUMER_KEY,
    consumerSecret: process.env.TWITTER_CONSUMER_SECRET,
    callback: process.env.TWITTER_CONSUMER_CALLBACK
});

var sess;

router.get('/', function (req, res) {
    sess=req.session;
    if(sess.accessTokenSecret){
        var viewData = {
            username: sess.username
        };
        res.render("twitter", viewData);
        return;
    }
    twitter.getRequestToken(function(error, requestToken, requestTokenSecret, results){
        if (error) {
            console.log("Error getting OAuth request token : " + error);
            res.redirect('/')
        } else {
            sess.requestToken=requestToken;
            sess.requestTokenSecret=requestTokenSecret;
            var uri = twitter.getAuthUrl(requestToken);
            res.redirect(uri)
        }
    });
});



router.get("/callback", function(req, res) {
    sess=req.session;
    var authReqData = req.query;
    sess.oauth_token = authReqData.oauth_token;
    sess.oauth_verifier = authReqData.oauth_verifier;

    twitter.getAccessToken(sess.requestToken, sess.requestTokenSecret, sess.oauth_verifier, function (error, accessToken, accessTokenSecret, results) {
        if (error) {
            console.log(error);
            res.redirect('/')
        } else {
            twitter.verifyCredentials(accessToken, accessTokenSecret, {}, function (error, data, response) {
                if (error) {
                    console.log(error);
                    res.redirect('/')
                } else {
                    sess.accessToken=accessToken;
                    sess.accessTokenSecret=accessTokenSecret;
                    sess.username = data["screen_name"]
                    res.redirect('/twitter')
                }
            });
        }
    });
});


router.post("/tweet", function(req, res){
    sess=req.session;
    var formula= req.body || {}

    math2ml.buildImage(formula, function(img){
        var params={
            media:img
        }
        twitter.uploadMedia(params, sess.accessToken, sess.accessTokenSecret, function (error,params_img) {
            if (error) {
                console.log(error);
                res.redirect('/')
            } else {
                var done = function (error, data, response) {
                    if (error) {
                        console.log(error);
                        res.redirect('/')
                    } else {
                        var viewData = {
                            username: data["screen_name"]
                        };
                        res.render("twitter", viewData);
                    }
                };
                var action = "update";
                var tweetData={
                    status: req.body.tweet,
                    media_ids:params_img.media_id_string
                };
                if( req.body.retweet ){
                    tweetData.in_reply_to_status_id = req.body.retweet;
                    tweetData.auto_populate_reply_metadata = true;
                }
                console.log(action)
                twitter.statuses(action, tweetData,sess.accessToken,sess.accessTokenSecret,done);
            }
        });
    });
});

module.exports = router;